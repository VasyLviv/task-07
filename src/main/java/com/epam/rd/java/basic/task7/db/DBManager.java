package com.epam.rd.java.basic.task7.db;

import java.io.*;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import com.epam.rd.java.basic.task7.db.entity.*;

public class DBManager {//implements InitializingBean {

    private static DBManager instance;
    private Properties prop = new Properties();
    private String URL;
    private String activeDataBase;
//	private Connection con;
//	private PreparedStatement stmt;
//	private ResultSet rs;


    private String toDerby(String str) {
        if (str.toLowerCase().contains("drop")) {
            str += ("  ifexists=true ");//RESTRICT ");
        }
        return str.replace("database", "SCHEMA")
                .replace("IF EXISTS", "")//""RESTRICT")
//                .replace("IF NOT EXISTS", "")//""RESTRICT")
                .replace("USE", "SET SCHEMA")
                .replace("AUTO_INCREMENT", "GENERATED ALWAYS AS IDENTITY not null")
                .replace("\u001a", "")
//                +";"
                ;
    }

    public void setActiveDataBase(String activeDataBase) {
        this.activeDataBase = activeDataBase;
    }

    public static synchronized DBManager getInstance() {
        try {
//            Class.forName("org.apache.derby.jdbc.EmbeddedDriver");//.getDeclaredConstructor();
//            Class.forName("org.apache.derby.jdbc.ClientDriver");//.getDeclaredConstructor();
            instance = new DBManager();
        } catch (DBException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return instance;
    }

    private DBManager() throws DBException {
        Connection conn;
        Statement stmt;
        ResultSet rs;
        try (InputStream inputProp = new FileInputStream("app.properties")) {
            this.prop.load(inputProp);
            URL = this.prop.getProperty("connection.url");
//            DriverManager.registerDriver(new org.apache.derby.jdbc.EmbeddedDriver());
//            Class.forName("org.apache.derby.jdbc.EmbeddedDriver");//.getDeclaredConstructor();
//            Class.forName("org.apache.derby.jdbc.ClientDriver");//.getDeclaredConstructor();

            try{
            conn = DriverManager.getConnection(URL);
            String s = new String();
            StringBuffer sb = new StringBuffer();

            FileReader fr = new FileReader(new File("sql/db-create.sql"));
            BufferedReader br = new BufferedReader(fr);
            while ((s = br.readLine()) != null) {
                sb.append(s);
            }
            br.close();
            String[] inst = sb.toString().split(";");

/////////////////////////////////////////////
//            try {
                DatabaseMetaData metas = conn.getMetaData();
                ResultSet tables = metas.getTables(conn.getCatalog(), "testdb", null, new String[]{"TABLE"});
        //        if (!tables.next())
                {
////////////////////////////////////////////


                    String sqlString;
                    stmt = conn.createStatement();

                    for (int i = 0; i < inst.length; i++) {
                        if (inst[i] != null
                                && !inst[i].trim().isEmpty()
                                && !inst[i].trim().equals("")
                                && !inst[i].trim().isBlank()
                        ) {
                            if (!inst[i].trim().toUpperCase().startsWith("SELECT")
//                    && !inst[i].trim().toUpperCase().startsWith("DROP")
                            ) {
//                        inst[i] = inst[i].replace("database", "schema");
                                sqlString = //toDerby(
                                        inst[i]//)
                                ;
                                try {
                                    stmt.executeUpdate(sqlString);
                                } catch (SQLException e) {
          //                          throw new DBException(e.getMessage(),e.getCause());
                                    System.out.println(e.getMessage()+" -> "+e.getCause());
                                }
                                if (inst[i].trim().toUpperCase().startsWith("SET")) {
                                    setActiveDataBase(toDerby(inst[i]));
                                }
                            }
//                    System.out.println(">>"+inst[i]);
                        }
                    }
//            System.out.println(">> sql file finished");
//            rs =    stmt.executeQuery(sb.toString());
//            rs.next();
//            ScriptRunner runner = new ScriptRunner(conn, [booleanAutoCommit], [booleanStopOnerror]);
//            runner.runScript(new BufferedReader(new FileReader("test.sql")));//            stmt = con.prepareStatement("USE test2db;");
/*            stmt.executeUpdate("ALTER TABLE users_teams\n" +
                    "   ADD CONSTRAINT FK_teams_usersteams_Cascade\n" +
                    "   FOREIGN KEY (team_id) REFERENCES teams(id) ON DELETE CASCADE;");
            stmt.executeUpdate("ALTER TABLE users_teams\n" +
                    "   ADD CONSTRAINT FK_users_usersteams_Cascade\n" +
                    "   FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE;");
*/
//            InputStream inputStream = getClass().getResourceAsStream("/sql/db-create.sql");
/*                SqlFile sqlFile = new SqlFile(new InputStreamReader(inputStream), "init", System.out, "UTF-8", false, new File("."));
                sqlFile.setConnection(connection);
                sqlFile.execute();
*/
                }
                conn.close();
            } catch (SQLException e) {
                throw new DBException(e.getMessage(), e.getCause());
//			throwables.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
        }
    }




    public List<User> findAllUsers() throws DBException {
        List<User> usersReturn = new ArrayList<>();

        String findAllUsers = "SELECT * FROM users";
        try {
            Connection conn = DriverManager.getConnection(URL);
            PreparedStatement stmt;
            ResultSet rs;
            if (activeDataBase != null) {
                stmt = conn.prepareStatement(activeDataBase);
                stmt.executeUpdate();
            }
            stmt = conn.prepareStatement(findAllUsers);
            rs = stmt.executeQuery();
            while (rs.next()) {
                User userToAdd = User.createUser(rs.getString("login"));
                userToAdd.setId(rs.getInt("id"));
                usersReturn.add(userToAdd);
            }
            conn.close();
        } catch (SQLException e) {
            throw new DBException("Finding allUsers after "
                    + usersReturn.get(usersReturn.size() - 1).getLogin()
                    + " -> " + e.getMessage()
                    , e.getCause());
            //throwables.printStackTrace();
        }
        return usersReturn;
    }

    public boolean insertUser(User user) throws DBException {
        boolean toReturn = false;
        String insertUser = "INSERT INTO users VALUES (DEFAULT, ?)";
        try {
            Connection conn = DriverManager.getConnection(URL);
            PreparedStatement stmt;
            ResultSet rs;
            if (activeDataBase != null) {
                stmt = conn.prepareStatement(activeDataBase);
                stmt.execute();
            }
            stmt = conn.prepareStatement(insertUser);
            stmt.setString(1, user.getLogin());
            stmt.execute();
            //rs.next();
            conn.close();
            user.setId(getUser(user.getLogin()).getId());
        } catch (SQLException e) {
//            throw new DBException("Adding user " + user + " -> " + e.getMessage()
//                    , e.getCause());
            //throwables.printStackTrace();
        }
        return true;
    }

    public boolean deleteUsers(User... users) throws DBException {
//        "DELETE  FROM test2db.users WHERE (id = '3');"
        boolean toReturn = false;
        StringBuilder deleteUsers = new StringBuilder("DELETE FROM users WHERE id IN (");//1,3,2);";
        try {
            Connection conn = DriverManager.getConnection(URL);
            PreparedStatement stmt;
            ResultSet rs;
            if (activeDataBase != null) {
                stmt = conn.prepareStatement(activeDataBase);
                stmt.executeUpdate();
            }
            for (User user : users) {
                deleteUsers.append(user.getId() + ",");
            }
            if (deleteUsers.length() > 0) {
                deleteUsers.replace(deleteUsers.length() - 1, deleteUsers.length() - 0, ")");
            }
            stmt = conn.prepareStatement(deleteUsers.toString());
//            stmt.setString(1, user.getLogin());
            stmt.executeUpdate();
            //rs.next();
            conn.close();
        } catch (SQLException e) {
            throw new DBException("Deleting users " + users + " -> " + e.getMessage()
                    , e.getCause());
            //throwables.printStackTrace();
        }
        return true;
    }

    public User getUser(String login) throws DBException {
        String findUser = "SELECT * FROM users WHERE (login = ?)";
        User userToReturn;
        try {
            Connection conn = DriverManager.getConnection(URL);
            PreparedStatement stmt;
            ResultSet rs;
            if (activeDataBase != null) {
                stmt = conn.prepareStatement(activeDataBase);
                stmt.executeUpdate();
            }
            stmt = conn.prepareStatement(findUser);
            stmt.setString(1, login);
            rs = stmt.executeQuery();
            rs.next();
            userToReturn = User.createUser(rs.getString("login"));
            userToReturn.setId(rs.getInt("id"));
            conn.close();
        } catch (SQLException e) {
            throw new DBException("Find user " + login + " -> " + e.getMessage()
                    , e.getCause());
        }
        return userToReturn;
    }

    public Team getTeam(String name) throws DBException {
        String findTeam = "SELECT * FROM teams WHERE (name = ?)";
        Team teamToReturn;
        try {
            Connection conn = DriverManager.getConnection(URL);
            PreparedStatement stmt;
            ResultSet rs;
            if (activeDataBase != null) {
                stmt = conn.prepareStatement(activeDataBase);
                stmt.executeUpdate();
            }
            stmt = conn.prepareStatement(findTeam);
            stmt.setString(1, name);
            rs = stmt.executeQuery();
            rs.next();
            teamToReturn = Team.createTeam(rs.getString("name"));
            teamToReturn.setId(rs.getInt("id"));
            conn.close();
        } catch (SQLException e) {
            throw new DBException("Find team " + name + " -> " + e.getMessage()
                    , e.getCause());
        }
        return teamToReturn;
    }

    public List<Team> findAllTeams() throws DBException {
        List<Team> teamsReturn = new ArrayList<>();

        String findAllTeams = "SELECT * FROM teams";
        try {
            Connection conn = DriverManager.getConnection(URL);
            PreparedStatement stmt;
            ResultSet rs;
            if (activeDataBase != null) {
                stmt = conn.prepareStatement(activeDataBase);
                stmt.executeUpdate();
            }
            stmt = conn.prepareStatement(findAllTeams);
            rs = stmt.executeQuery();
            while (rs.next()) {
                Team teamToAdd = Team.createTeam(rs.getString("name"));
                teamToAdd.setId(rs.getInt("id"));
                teamsReturn.add(teamToAdd);
            }
            conn.close();
        } catch (SQLException e) {
            throw new DBException("Finding allUsers after "
                    + teamsReturn.get(teamsReturn.size() - 1).getName()
                    + " -> " + e.getMessage()
                    , e.getCause());
            //throwables.printStackTrace();
        }
        return teamsReturn;
    }

    public boolean insertTeam(Team team) throws DBException {
        boolean toReturn = false;
        String insertTeam = "INSERT INTO teams VALUES (DEFAULT, ?)";
        try {
            Connection conn = DriverManager.getConnection(URL);
            PreparedStatement stmt;
            ResultSet rs;
            if (activeDataBase != null) {
                stmt = conn.prepareStatement(activeDataBase);
                stmt.executeUpdate();
            }
            stmt = conn.prepareStatement(insertTeam);
            stmt.setString(1, team.getName());
            stmt.executeUpdate();
            //rs.next();
            conn.close();
            team.setId(getTeam(team.getName()).getId());
        } catch (SQLException e) {
            throw new DBException("Adding user " + team + " -> " + e.getMessage()
                    , e.getCause());
            //throwables.printStackTrace();
        }
        return true;
    }

    public boolean setTeamsForUser(User user, Team... teams) throws DBException {
        boolean toReturn = false;
        int insertResult = 0;
        String insertTeamsForUsers = "INSERT INTO users_teams VALUES ( ?, ?)";
        try {
            Connection conn = DriverManager.getConnection(URL);
            PreparedStatement stmt;
            ResultSet rs;
            if (activeDataBase != null) {
                stmt = conn.prepareStatement(activeDataBase);
                stmt.executeUpdate();
            }
            for (Team team : teams) {
                stmt = conn.prepareStatement(insertTeamsForUsers);
                stmt.setInt(1, user.getId());
                stmt.setInt(2, team.getId());
                insertResult += stmt.executeUpdate();
            }
            //rs.next();
            conn.close();
        } catch (SQLException e) {
            throw new DBException("Adding " + teams + " to " + user
                    + " -> " + e.getMessage()
                    , e.getCause());
            //throwables.printStackTrace();
        }
        return insertResult > 0 ? true : false;
    }

    public List<Team> getUserTeams(User user) throws DBException {
        List<Team> teamsToReturn = new ArrayList<>();
        int selectResult = 0;
        String selectTeamsForUsers = "SELECT ut.user_id, ut.team_id, t.id, t.name"
                + " FROM testdb.users_teams AS ut"
                + " LEFT JOIN testdb.teams AS t"
                + " ON ut.team_id = t.id"
                + " WHERE ut.user_id = ?"
//                + " ;"
                ;
        try {
            Connection conn = DriverManager.getConnection(URL);
            PreparedStatement stmt;
            ResultSet rs;
            if (activeDataBase != null) {
                stmt = conn.prepareStatement(activeDataBase);
                stmt.executeUpdate();
            }
            stmt = conn.prepareStatement(selectTeamsForUsers);
            stmt.setInt(1, user.getId());
            rs = stmt.executeQuery();
            while (rs.next()) {
                Team teamToAdd = Team.createTeam(rs.getString("name"));
                teamToAdd.setId(rs.getInt("id"));
                teamsToReturn.add(teamToAdd);
            }
            conn.close();
        } catch (SQLException e) {
            throw new DBException("finding teams for  " + user
                    + " -> " + e.getMessage()
                    , e.getCause());
            //throwables.printStackTrace();
        }
        return teamsToReturn;
    }

    public boolean deleteTeam(Team team) throws DBException {
        boolean toReturn = false;
        StringBuilder deleteUsers = new StringBuilder("DELETE FROM teams WHERE id = ?");
        try {
            Connection conn = DriverManager.getConnection(URL);
            PreparedStatement stmt;
            ResultSet rs;
            if (activeDataBase != null) {
                stmt = conn.prepareStatement(activeDataBase);
                stmt.executeUpdate();
            }
            stmt = conn.prepareStatement(deleteUsers.toString());
            stmt.setInt(1, team.getId());
            if (stmt.executeUpdate() > 0) {
                toReturn = true;
            }
            ;
            //rs.next();
            conn.close();
        } catch (SQLException e) {
            throw new DBException("Deleting team " + team + " -> " + e.getMessage()
                    , e.getCause());
            //throwables.printStackTrace();
        }
        return toReturn;
    }

    public boolean updateTeam(Team team) throws DBException {
        boolean toReturn = false;
        StringBuilder deleteUsers = new StringBuilder("UPDATE teams\n" +
                "SET name = ? WHERE id = ?");
        try {
            Connection conn = DriverManager.getConnection(URL);
            PreparedStatement stmt;
            ResultSet rs;
            if (activeDataBase != null) {
                stmt = conn.prepareStatement(activeDataBase);
                stmt.executeUpdate();
            }
            stmt = conn.prepareStatement(deleteUsers.toString());
            stmt.setString(1, team.getName());
            stmt.setInt(2, team.getId());
            if (stmt.executeUpdate() > 0) {
                toReturn = true;
            }
            //rs.next();
            conn.close();
        } catch (SQLException e) {
            throw new DBException("Deleting team " + team + " -> " + e.getMessage()
                    , e.getCause());
            //throwables.printStackTrace();
        }
        return toReturn;
    }

}
